import curses
import time
import RPi.GPIO as GPIO
import random

# GPIO pin declaration
ENA = 12 
ENB = 16
IN1 = 7
IN2 = 11
IN3 = 13
IN4 = 15
ECHO = 18
TRIG = 22

# Script mode
demo = 1

# PWM values
dc = 20
freq = 15

def init():
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(ENA, GPIO.OUT)
        GPIO.setup(IN1, GPIO.OUT)
        GPIO.setup(IN2, GPIO.OUT)
        GPIO.setup(IN3, GPIO.OUT)
        GPIO.setup(IN4, GPIO.OUT)
        GPIO.setup(ENB, GPIO.OUT)
        GPIO.setup(TRIG, GPIO.OUT)
        GPIO.setup(ECHO, GPIO.IN)

def forward():
        GPIO.output(IN1, 1)
        GPIO.output(IN2, 0)
        GPIO.output(IN3, 1)
        GPIO.output(IN4, 0)

def right():
        GPIO.output(IN1, 1)
        GPIO.output(IN2, 0)
        GPIO.output(IN3, 0)
        GPIO.output(IN4, 1)

def backward():
        GPIO.output(IN1, 0)
        GPIO.output(IN2, 1)
        GPIO.output(IN3, 0)
        GPIO.output(IN4, 1)

def left():
        GPIO.output(IN1, 0)
        GPIO.output(IN2, 1)
        GPIO.output(IN3, 1)
        GPIO.output(IN4, 0)

def stop():
        GPIO.output(IN1, 0)
        GPIO.output(IN2, 0)
        GPIO.output(IN3, 0)
        GPIO.output(IN4, 0)


def distance():
        # Settling sensor
        GPIO.output(TRIG, 0)
        time.sleep(2)

        # Trigger pulse
        GPIO.output(TRIG, 1)
        time.sleep(0.00001)
        GPIO.output(TRIG, 0)

        while GPIO.input(ECHO) == 0:
                pulse_start = time.time()

        while GPIO.input(ECHO) == 1:
                pulse_end = time.time()

        pulse_duration = pulse_end - pulse_start

        distance = round(pulse_duration * 17150, 2)

        return distance

def demo_zero():
        screen = curses.initscr(); curses.noecho(); curses.cbreak(); screen.keypad(True)

        while True:
                char = screen.getch()

                if char == ord('q'):
                        break
                elif char == curses.KEY_UP:
                        screen.addstr(0, 0, 'forward')
                        forward()

                elif char == curses.KEY_RIGHT:
                        screen.addstr(0, 0, 'right  ')
                        right()
      
                elif char == curses.KEY_DOWN:
                        screen.addstr(0, 0, 'down   ')
                        backward()
       
                elif char == curses.KEY_LEFT:
                        screen.addstr(0, 0, 'left   ')
                        left()

                elif char == 10:
                        screen.addstr(0, 0, 'stop   ')
                        stop()

        curses.nocbreak(); screen.keypad(0); curses.echo(); curses.endwin()

def demo_one():
        while True:
                dis = distance()

                print "distance: " + str(dis)

                if dis > 90:
                        forward()
                        print "direction: forward"
                else:
                        backward()
                        print "direction: backward"

                        if random.randint(0,1) == 0:
                                right()
                                print "direction: right"
                        else:
                                left()
                                print "direction: left"
init()

#GPIO.output(ENA, 1); GPIO.output(ENB, 1)
# Uncomment to enable PWM
ENA_PWM = GPIO.PWM(ENA, freq); ENB_PWM = GPIO.PWM(ENB, freq)

try:
        # Uncomment to enable PWM
        ENA_PWM.start(dc); ENB_PWM.start(dc)

        if demo == 0:
                demo_zero()
        elif demo == 1:
                demo_one()

finally:
        #GPIO.output(ENA, 0); GPIO.output(ENB, 0)

        # Uncomment to enable PWM
        ENA_PWM.stop(); ENB_PWM.stop()

        GPIO.cleanup()
